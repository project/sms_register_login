## Register Route:
/user/sms-register

## Install steps:
1. Enable sms_user
2. Enable sms_register_login

Features:
1.  Verification code can't been sent repetitively within 60 seconds.

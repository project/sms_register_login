<?php

namespace Drupal\sms_register_login\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\PasswordConfirm;
use Drupal\sms_register_login\SmsRegisterLoginFormBase;
use Drupal\user\Entity\User;

/**
 * Class SmsRegisterForm.
 */
class SmsRegisterForm extends SmsRegisterLoginFormBase {

  /**
   * Build the form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#dedault_value' => $form_state->getUserInput()['mail'],
    ];

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];

    $class = get_class($this);
    $form['pass'] = [
      '#type' => 'password_confirm',
      '#size' => 25,
      '#required' => TRUE,
      '#process' => [
        [$class, 'processPasswordConfirm'],
      ],
    ];

    $form['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone Number'),
      '#required' => TRUE,
      '#dedault_value' => $form_state->getUserInput()['phone_number'],
    ];

    // Button.
    $form['send_code'] = [
      '#type' => 'button',
      '#value' => $this->t('Send Verification Code'),
    ];
    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Verification Code'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Register'),
    ];

    return $form;
  }

  /**
   * Expand a password_confirm field into two text boxes.
   */
  public static function processPasswordConfirm(&$element, FormStateInterface $form_state, &$complete_form) {
    $element = PasswordConfirm::processPasswordConfirm($element, $form_state, $complete_form);
    $user_input = $form_state->getUserInput();
    if (!empty($user_input['pass']['pass1'])) {
      $element['pass1']['#attributes']['value'] = $user_input['pass']['pass1'];
    }
    if (!empty($user_input['pass']['pass2'])) {
      $element['pass2']['#attributes']['value'] = $user_input['pass']['pass2'];
    }
    return $element;
  }

  /**
   * Getter method for Form ID.
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller. It must be
   * unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'sms_register';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $phone = $form_state->getValue('phone_number');
    if (!preg_match("/^1[345678]{1}\d{9}$/", $phone)) {
      $form_state->setErrorByName('phone_number', $this->t('%phone is an invalid phone number.', ['%phone' => $phone]));
      // $form_state->setErrorByName($form['phone_number'], $this->t('phone is an invalid phone number.'));
      return;
    }
    $mail = $form_state->getValue('mail');
    $name = $form_state->getValue('name');
    /** @var \Drupal\user\UserStorage $user_storage */
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');
    if (!\Drupal::service('email.validator')->isValid($mail)) {
      $form_state->setErrorByName('mail', $this->t('%email is an invalid email address.', ['%email' => $mail]));
      return;
    }
    // Check if the email has been registered by an existing active account.
    $find_user_by_email = $user_storage->loadByProperties(['mail' => $mail]);
    if (!empty($find_user_by_email)) {
      $form_state->setErrorByName('mail', $this->t('The email has been registered by an active user.'));
      return;
    }

    // Check if the email has been registered by an existing active account.
    $find_user_by_email = $user_storage->loadByProperties(['name' => $name]);
    if (!empty($find_user_by_email)) {
      $form_state->setErrorByName('name', $this->t('The username has been used.'));
      return;
    }

    // Send verification code then use click the button.
    if ($form_state->getTriggeringElement()['#id'] == 'edit-send-code') {
      // Check if the email has been registered by an existing active account.
      $user = $user_storage->loadByProperties(['phone_number' => $phone]);
      $user = reset($user);
      if (empty($user)) {
        $user = User::create([
          'phone_number' => $phone,
          'name' => \Drupal::service('uuid')->generate(),
        ]);
        $user->save();
      }
      $form_state->set('user', $user);
      \Drupal::service('sms_register_login.sms')->sendCodeToUser($user);
      $form_state->setTriggeringElement(NULL);
    }

    // When user click submit button.
    if ($form_state->getTriggeringElement()['#id'] == 'edit-submit') {
      $verify_code = $name = $form_state->getValue('code');
      if (empty($verify_code)) {
        $form_state->setErrorByName('code', $this->t("The verification code can't be null."));
        return;
      }
      $this->validatePhoneCodeForm($form, $form_state);

    }
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $code = $form_state->getValue('code');
    $phone_verification = $this->phoneNumberVerification
      ->getPhoneVerificationByCode($code);
    $phone_verification
      ->setStatus(TRUE)
      ->setCode('')
      ->save();
    $this->messenger()->addMessage($this->t('Phone number is now verified.'));

    /** @var \Drupal\user\Entity\User $user */
    $user = $form_state->get('user');
    $mail = $form_state->getValue('mail');
    $name = $form_state->getValue('name');
    $pass = $form_state->getValue('pass');
    $user->set('mail', $mail);
    $user->set('name', $name);
    $user->setPassword($pass);
    $user->activate();
    $user->save();
    user_login_finalize($user);
    // $form_state->setRedirect('<front>');
    $form_state->setRedirect('cgzhj_membership.member');
  }

}

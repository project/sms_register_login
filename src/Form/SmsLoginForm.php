<?php

namespace Drupal\sms_register_login\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\sms_register_login\SmsRegisterLoginFormBase;

/**
 * Class SmsLoginForm.
 */
class SmsLoginForm extends SmsRegisterLoginFormBase {

  /**
   * Build the form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['phone_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone'),
      '#required' => TRUE,
      '#dedault_value' => isset($form_state->getUserInput()['phone']) ? $form_state->getUserInput()['phone'] : NULL,
    ];

    // Button.
    $form['send_code'] = [
      '#type' => 'button',
      '#value' => $this->t('Send Verification Code'),
    ];
    $form['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone Verification Code'),
      '#size' => 60,
      '#maxlength' => 128,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Login'),
    ];

    return $form;
  }

  /**
   * Getter method for Form ID.
   */
  public function getFormId() {
    return 'sms_login';
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $phone = $form_state->getValue('phone_number');
    /** @var \Drupal\user\UserStorage $user_storage */
    $user_storage = \Drupal::entityTypeManager()->getStorage('user');

    // Send verification code then use click the button.
    if ($form_state->getTriggeringElement()['#id'] == 'edit-send-code') {
      // Check if the email has been registered by an existing active account.
      $user = $user_storage->loadByProperties(['phone_number' => $phone, 'status' => 1]);
      $user = reset($user);
      if (empty($user)) {
        $form_state->setErrorByName('phone_number', $this->t("The phone didn't bind to any active account, please register first."));
        return;
      }
      $form_state->set('user', $user);
      \Drupal::service('sms_register_login.sms')->sendCodeToUser($user);
    }

    // When user click submit button.
    if ($form_state->getTriggeringElement()['#id'] == 'edit-submit') {
      $verify_code = $name = $form_state->getValue('code');
      if (empty($verify_code)) {
        $form_state->setErrorByName('code', $this->t("The verification code can't be null."));
        return;
      }
      $this->validatePhoneCodeForm($form, $form_state);

    }
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $code = $form_state->getValue('code');
    $phone_verification = $this->phoneNumberVerification
      ->getPhoneVerificationByCode($code);
    $phone_verification
      ->setStatus(TRUE)
      ->setCode('')
      ->save();
    $this->messenger()->addMessage($this->t('Phone number is now verified.'));

    /** @var \Drupal\user\Entity\User $user */
    $user = $form_state->get('user');
    user_login_finalize($user);
    $form_state->setRedirect('<front>');
  }

}

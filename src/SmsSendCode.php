<?php

namespace Drupal\sms_register_login;

/**
 * Class SmsSendCodeToPhone.
 */
class SmsSendCode {

  /**
   * Send code to phone.
   */
  public function sendCodeToUser($user) {
    $phone = $user->phone_number->first()->value;

    if (empty($phone)) {
      \Drupal::messenger()->addError(t("User doesn't has phone number"));
      return;
    }

    try {
      $send = $this->generateNewCode($user, $phone);
      if ($send) {
        \Drupal::messenger()->addMessage(t('The request of sending verification code to @phone has been executed', ['@phone' => $phone]));
      }
    }
    catch (\Exception $e) {
      throw new \Exception($e->getMessage());
    }
  }

  /**
   * Generate the new code.
   *
   * @param $sms_placeholder
   * @param $phone
   *
   * @return string
   */
  public function generateNewCode($sms_placeholder, $phone) {
    /** @var \Drupal\sms\Provider\PhoneNumberVerificationInterface $phone_number_verification_provider */
    $phone_number_verification_provider = \Drupal::service('sms.phone_number.verification');
    /** @var \Drupal\sms\Entity\PhoneNumberVerification $last_phone_number_verification */
    $last_phone_number_verification = $phone_number_verification_provider->getPhoneVerificationByEntity($sms_placeholder, $phone);
    $last_create_time = $last_phone_number_verification->getCreatedTime();
    $current_time = time();
    $time_past = $current_time - $last_create_time;
    if ($time_past <= 60) {
      \Drupal::messenger()->addWarning(t("Verification code can't been sent repetitively within 60 seconds，Please try again later"));
      return FALSE;
    }
    // Delete the existing entity:sms_phone_number_verification for $sms_placeholder.
    $phone_number_verification_provider->deletePhoneVerificationByEntity($sms_placeholder);

    // Create a new entity:sms_phone_number_verification for $sms_placeholder
    // with the phone number. The new sms_phone_number_verification will
    // contain a "new code" for authentication, at this time, the
    // sms_phone_number_verification's status is false, after user use the
    // "new code" on page /verify to verify it.
    // the new sms_phone_number_verification's status will become TRUE.
    // Important: This line will send code to the phone.
    /** @var \Drupal\sms\Entity\PhoneNumberVerificationInterface $verification */
    $phone_number_verification_provider->newPhoneVerification($sms_placeholder, $phone);
    return TRUE;
  }

}
